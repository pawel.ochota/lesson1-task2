const car = require('..');

describe('Object car', () => {
  it('should have a field "name" with "Toyota Yaris" value', async () => {
    expect(typeof car.name).toBe('string');
    expect(car.name.toLowerCase()).toBe('toyota yaris');
  });

  it('should have a field "price" with 50000 value', async () => {
    expect(typeof car.price).toBe('number');
    expect(car.price).toBe(50000);
  });

  it('should have a field "color" with "blue" value', async () => {
    expect(typeof car.color).toBe('string');
    expect(car.color).toBe('blue');
  });

  it('should have a field "led" with true value', async () => {
    expect(typeof car.led).toBe('boolean');
    expect(car.led).toBeTruthy();
  });
});
